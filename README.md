Plot the streamgraph of ATFM delay causes and print highlighted value under the cursor.

Mar-2014 - Jul-2016
Done:
* extract the data from http://ansperformance.eu
* plug the data
* then the miracle happens

TODO:
* re-enable and re-work mouse hover
* check data for NA on dates
* x-axis ticks

forked from/inspired by <a href='http://bl.ocks.org/biovisualize/'>biovisualize</a>'s block: <a href='http://bl.ocks.org/biovisualize/eb1d77fdb9a2b0670381'>Streamgraph extent under cursor</a>